import Head from 'next/head';
import Image from 'next/image';
import { Select, Table } from 'antd';
import RESOURCE_LIST from "../constants/resourceList";
import {
  BLUE_WOLF_MATS_LIST,
  BLUE_WOLF_ROBE_MATS_LIST,
  BLUE_WOLF_HEAVY_MATS_LIST,
  BLUE_WOLF_LIGHT_MATS_LIST,
} from "../constants/blueWolf";
import {
  AVADON_MATS_LIST,
  AVADON_ROBE_MATS_LIST,
  AVADON_HEAVY_MATS_LIST,
  AVADON_LIGHT_MATS_LIST,
} from '../constants/avadon';
import LVL_LIST from "../constants";
import bem from '../utils/bem';
import styles from '../styles/Home.module.scss';
import {useMemo, useState} from "react";

const b = bem('home', styles);

const TYPES_LIST = [
  {
    value: 'heavy',
    label: 'Хеви сеты',
    sets: [...BLUE_WOLF_HEAVY_MATS_LIST, ...BLUE_WOLF_MATS_LIST, ...AVADON_HEAVY_MATS_LIST, ...AVADON_MATS_LIST]
  },
  {
    value: 'light',
    label: 'Лайт части',
    sets: [...BLUE_WOLF_LIGHT_MATS_LIST, ...BLUE_WOLF_MATS_LIST, ...AVADON_LIGHT_MATS_LIST, ...AVADON_MATS_LIST]
  },
  {
    value: 'robe',
    label: 'Робы',
    sets: [...BLUE_WOLF_ROBE_MATS_LIST, ...BLUE_WOLF_MATS_LIST, ...AVADON_ROBE_MATS_LIST, ...AVADON_MATS_LIST]
  },
  {
    value: 'res',
    label: 'Ресурсы',
    sets: RESOURCE_LIST,
  },
]

export default function Home() {
  const [selectedRes, selectRes] = useState(null);
  const [minLvl, onChangeMin] = useState(1);
  const [maxLvl, onChangeMax] = useState(80);
  const [pageSize, onChangePageSize] = useState(10);
  const [showingList, onChangeList] = useState([]);
  const [dropType, onChangeDropType] = useState('spoil');

  const columns = [
    {
      title: 'Имя',
      dataIndex: 'name',
      key: 'name',
      render: (value) => <a>{value}</a>,
    },
    {
      title: 'Левел',
      dataIndex: 'lvl',
      key: 'lvl',
      render: (value) => <a>{value}</a>,
    },
    {
      title: 'Мин. кол-во',
      dataIndex: 'minQuantity',
      key: 'minQuantity',
      render: (minQuantity) => <a>{minQuantity}</a>,
      sorter: {
        compare: (a, b) => a.minQuantity - b.minQuantity,
        multiple: 2,
      },
    },
    {
      title: 'Макс. кол-во',
      dataIndex: 'maxQuantity',
      key: 'maxQuantity',
      render: (maxQuantity) => <a>{maxQuantity}</a>,
      sorter: {
        compare: (a, b) => a.maxQuantity - b.maxQuantity,
        multiple: 2,
      },
    },
    {
      title: 'Шанс',
      dataIndex: 'chance',
      key: 'chance',
      render: (value) => <a>{`${value}%`}</a>,
      sorter: {
        compare: (a, b) => a.chance - b.chance,
        multiple: 3,
      },
    },
  ];

  const maxChanceSorted = useMemo(() => {
    return showingList.map(item => {
      const minMaxDrop = item.drop.map(spoilItem => {
        const { quantity } = spoilItem;
        const minVal = quantity.slice(0, 1);
        const maxVal = quantity.split('-')[1];
        if (!maxVal) {
          return {
            ...spoilItem,
            minQuantity: minVal,
            maxQuantity: minVal,
          }
        }
        return {
          ...spoilItem,
          minQuantity: minVal,
          maxQuantity: maxVal,
        }
      })
      const minMaxSpoil = item.spoil.map(spoilItem => {
        const { quantity } = spoilItem;
        const minVal = quantity.slice(0, 1);
        const maxVal = quantity.split('-')[1];
        if (!maxVal) {
          return {
            ...spoilItem,
            minQuantity: minVal,
            maxQuantity: minVal,
          }
        }
        return {
          ...spoilItem,
          minQuantity: minVal,
          maxQuantity: maxVal,
        }
      })
      return {
        ...item,
        drop: minMaxDrop,
        spoil: minMaxSpoil,
      }
    })
  }, [showingList, dropType])

  const onChangeRes = (id) => {
    const res = maxChanceSorted.find(({ value }) => id === value);
    selectRes(res);
  }

  const levelList = LVL_LIST.map(item => ({ value: item.value, label: `${item.value}` }))

  const filteredList = selectedRes ? selectedRes[dropType].filter(({ lvl }) => (lvl >= minLvl && lvl <= maxLvl)) : [];

  const onShowSizeChange = (_, pageSize) => {
    onChangePageSize(pageSize)
  };

  return (
    <div>
      <Head>
        <title>L2 item helper</title>
        <meta name="description" content="Created by Igor Gritsaenko" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={b()}>
        <div className={b('main-block')}>
          <div className={b('main-block-title')}>Тип поиска</div>
          <Select
            className={b('main-block-select')}
            showSearch
            placeholder="Тип поиска"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
            }
            onChange={(_, value) => {
              onChangeList(value.sets)
            }}
            options={TYPES_LIST}
          />
          {
            showingList.length ? (
               <>
                 <div className={b('main-block-title')}>Выбирете ресурс</div>
                 <Select
                   className={b('main-block-select')}
                   showSearch
                   placeholder="Выбери ресурс"
                   optionFilterProp="children"
                   filterOption={(input, option) =>
                     (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                   }
                   onChange={onChangeRes}
                   options={maxChanceSorted}
                 />
                 <div className={b('main-block-title')}>Дроп или споил</div>
                 <Select
                   className={b('main-block-select')}
                   showSearch
                   defaultValue={dropType}
                   placeholder="Тип поиска"
                   optionFilterProp="children"
                   filterOption={(input, option) =>
                     (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                   }
                   onChange={(_, value) => {
                     onChangeDropType(value.value)
                   }}
                   options={[{ value: 'spoil', label: 'Споил'}, { value: 'drop', label: 'Дроп'}]}
                 />
               </>
            ) : null
          }
         <div className={b('selectors-list')}>
           <div>
             <div className={b('main-block-title')}>Выбирете мин лвл</div>
             <Select
               className={b('main-block-select', { min: true })}
               showSearch
               defaultValue={levelList[0]}
               placeholder="Выбери мин лвл"
               optionFilterProp="children"
               filterOption={(input, option) =>
                 (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
               }
               onChange={onChangeMin}
               options={levelList}
             />
           </div>
           <div>
             <div className={b('main-block-title')}>Выбирете макс лвл</div>
             <Select
               className={b('main-block-select', { min: true })}
               showSearch
               defaultValue={levelList[levelList.length-1]}
               placeholder="Выбери макс лвл"
               optionFilterProp="children"
               filterOption={(input, option) =>
                 (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
               }
               onChange={onChangeMax}
               options={levelList}
             />
           </div>
         </div>
        </div>
        {
          selectedRes ? (
            <div className={b('selected-res')}>
              <div className={b('selected-res-image')}>
                <Image
                  className={b('res-image')}
                  src={selectedRes.image}
                  alt="resource"
                  fill
                />
              </div>
              <div className={b('selected-res-name')}>{selectedRes.label}</div>
              <a className={b('selected-res-link')} href={selectedRes.link}>Link</a>
            </div>
          ) : null
        }
        <div className={b('mobs-list')}>
          <Table
            dataSource={filteredList}
            columns={columns}
            pagination={{
              pageSize,
              showSizeChanger: true,
              onShowSizeChange,
            }}
          />
        </div>
      </main>
    </div>
  )
}
