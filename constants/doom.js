const DOOM_ROBE_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'robe',
		spoil: [

		],
		drop: [

		]
	},
];

const DOOM_LIGHT_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'light',
		spoil: [

		],
		drop: [

		]
	},
]

const DOOM_HEAVY_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'heavy',
		spoil: [

		],
		drop: [

		]
	},
]

const DOOM_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: '',
		spoil: [

		],
		drop: [

		]
	},
]

export {
	DOOM_MATS_LIST,
	DOOM_ROBE_MATS_LIST,
	DOOM_HEAVY_MATS_LIST,
	DOOM_LIGHT_MATS_LIST,
};
