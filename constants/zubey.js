const ZUBEY_ROBE_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'robe',
		spoil: [

		],
		drop: [

		]
	},
];

const ZUBEY_LIGHT_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'light',
		spoil: [

		],
		drop: [

		]
	},
]

const ZUBEY_HEAVY_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: 'heavy',
		spoil: [

		],
		drop: [

		]
	},
]

const ZUBEY_MATS_LIST = [
	{
		value: 1,
		image: '',
		label: '',
		link: '',
		mass: '',
		spoil: [

		],
		drop: [

		]
	},
]

export {
	ZUBEY_MATS_LIST,
	ZUBEY_ROBE_MATS_LIST,
	ZUBEY_HEAVY_MATS_LIST,
	ZUBEY_LIGHT_MATS_LIST,
};
