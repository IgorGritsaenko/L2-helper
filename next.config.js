/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['mw2.global'],
  },
}

module.exports = nextConfig
